let cantidad=document.getElementById('cantidad').value;
let comision=document.getElementById('totalComision').value;
let subtotal=document.getElementById('subtotal').value;
let mon=document.getElementById('monedaOrigen').value;
let monDes=document.getElementById('monedaDestino').value;

let tasa=0;
switch(monOrigen){
	case'1':
    switch(monDestino){
        case '1':
        tasa=1;
        break;
        case'2':
        tasa=1.35;
        break;
        case'3':
        tasa=19.85;
        break;
        case'4':
        tasa=0.44;
        break;
    }
    break;

    case '2':
    switch(monDestino){
        case'1':
        tasa=0.4;
        break;
        case'2':
        tasa=1;
        break;
        case'3':
        tasa=14.71;
        break;
        case'4':
        tasa=0.73;
        break;
    }
    break;

    case '3':
    switch(monDestino){
        case'1':
        tasa=1.01;
        break;
        case'2':
        tasa=0.068;
        break;
        case'3':
        tasa=1;
        break;
        case'4':
        tasa=0.050;
        break;
    }
    break;

    case '4':
        switch(monDestino){
            case'1':
            tasa=1.01;
            break;
            case'2':
            tasa=1.37;
            break;
            case'3':
            tasa=20.13;
            break;
            case'4':
            tasa=1;
            break;
        }
        break;
}

subtotal=cantidad*tasa;
comision=subtotal*0.3;
total=subtotal+comision;
document.getElementById('subtotal').value=(subtotal).toFixed(2);
document.getElementById('totalComision').value=(comision).toFixed(2);
document.getElementById('totalPago').value=(total).toFixed(2);
